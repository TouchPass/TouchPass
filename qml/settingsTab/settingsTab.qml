import QtQuick 2.4
import Ubuntu.Components 1.3
import "../components"

Page {
    id: pageSettings
    anchors.fill: parent
    objectName: "SettingsPage"

    header: MainPageHeader {
        id: header
        pageTitle: i18n.tr("Settings")
    }

    ScrollView {
        id: settingsView
        anchors.fill: parent
        anchors.top: header.bottom
        anchors.topMargin: units.gu(5)

        Column {
            width: settingsView.width

            SettingsItem {
                title.text: i18n.tr("Automatic saving database")
                summary.text: i18n.tr("If enable, after modifying entry, the database will be automatically save")
                summary.maximumLineCount: Number.MAX_VALUE
                visible: true

                control: Switch {
                    Component.onCompleted: checked = settings.autoSaveFile
                    onClicked: {
                        settings.autoSaveFile = !settings.autoSaveFile
                    }
                }
            }
            SettingsItem {
                title.text: i18n.tr("Auto-close database after inactivity")
                summary.text: i18n.tr("In minutes. 0 for disabled.")
                summary.maximumLineCount: Number.MAX_VALUE
                visible: true

                control: TextField {
                    inputMethodHints: Qt.ImhDigitsOnly
                    text: settings.autoCloseInterval
                    onTextChanged: {
                        if (isNaN(parseInt(text))) {
                            text = 1
                        }
                        if (parseInt(text) < 0) {
                            text = 1
                        }
                        if (parseInt(text) > 100) {
                            text = 100
                        }

                        settings.autoCloseInterval = parseInt(text)
                    }
                    hasClearButton: false
                    validator: IntValidator {
                        bottom: 0
                        top: 100
                    }
                }
            }
        }
    }
}