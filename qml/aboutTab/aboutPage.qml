import QtQuick 2.4
import QtGraphicalEffects 1.0
import Ubuntu.Components 1.3 as UITK
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

UITK.Page {
    id: aboutPageRootItem
    anchors.fill: parent
    objectName: "AboutPage"

    header: UITK.PageHeader {
        id: header
        opacity: 1
        title: i18n.tr('About')
    }
    Flickable {
        id: flickable
        anchors.fill: parent
        contentHeight: layout.height + units.gu(10)

        Column {
            id: layout

            spacing: units.gu(3)
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                topMargin: units.gu(10)
            }

            Column {
                width: parent.width
                UITK.Label {
                    width: parent.width
                    textSize: UITK.Label.XLarge
                    font.weight: Font.DemiBold
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                    text: i18n.tr("TouchPass")
                }
                UITK.Label {
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    text: i18n.tr("Version 1.0.0")
                }
            }

            UITK.Label {
                width: parent.width
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                text: i18n.tr("A KeePass application for Ubuntu Touch.<br/>This application is inspired by the already existing application KeePass by David Ventura.")
            }

            Column {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: units.gu(2)
                }

                UITK.Label {
                    width: parent.width
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter
                    text: i18n.tr("It uses the Python library pykeepass which is slower than that of the original KeePass application to open the database, but it allows to modify entries.")
                }

                UITK.Label {
                    width: parent.width
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter
                    text: i18n.tr("(C) 2023 Pierre VERBAERE<br/>")
                }

                UITK.Label {
                    textSize: UITK.Label.Small
                    width: parent.width
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter
                    text: i18n.tr("Released under the terms of the MIT LICENSE")
                }
            }

            UITK.Label {
                width: parent.width
                wrapMode: Text.WordWrap
                textSize: UITK.Label.Small
                horizontalAlignment: Text.AlignHCenter
                linkColor: theme.palette.normal.focus
                text: i18n.tr("Source code available on %1").arg("<a href=\"https://gitea.verbaere.com/TouchPass/TouchPass\">Git</a>")
                onLinkActivated: Qt.openUrlExternally(link)
            }

        }
    }

}
