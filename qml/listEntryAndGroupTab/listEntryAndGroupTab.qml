import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as UbuntuListItems
import io.thp.pyotherside 1.4
import "../components"

Page {
    id: pageListEntry
    anchors.fill: parent
    objectName: "ListEntryPage"

    property string actualPath

    header: MainPageHeader {
        id: header
        pageTitle: i18n.tr("Database content")
    }

    ActivityIndicator {
        id: activityIndicator
        anchors.centerIn: parent
        visible: true
        running: true
    }

    ScrollView {
        id: listEntryView
        anchors.fill: parent
        anchors.top: header.bottom
        anchors.topMargin: units.gu(5)
        anchors.bottomMargin: units.gu(3)

        ListView {
            id: entryListView
            anchors.fill: parent

            model: ListModel {
                id: listmodel
            }

            delegate: ListItem {
                height: units.gu(9)
                onClicked: {
                        if(model.type == "group" || model.type == "back") {
                            python.open_group(model.path)
                        } else {
                            pageStack.push(Qt.resolvedUrl("../entryPage/entryPage.qml"), { path: model.path })
                        }
                        
                    }

                ListItemLayout {
                    anchors.fill: parent

                    title.text: model.title

                    Icon {
                        SlotsLayout.position: SlotsLayout.Leading
                        width: units.gu(5); height: width
                        name: model.type == "group" ? "folder-symbolic" : model.type == "back" ? "back" :"stock_key"
                    }

                    Icon {
                        SlotsLayout.position: SlotsLayout.Last
                        width: units.gu(2); height: width
                        name: "go-next"
                        visible : model.type == "back" ? false : true
                    }
                }
            }
        }
    }
    Component.onCompleted: {
        if(databaseOpen) {
            python.open_group()
        }
    }
    BottomEdge {
        id: bottomEdge
        preloadContent: false
        height: root.height
        contentUrl: Qt.resolvedUrl("../addEntryPage/addEntryPage.qml")
        hint.visible: enabled

        BottomEdgeHint {
            id: bottomEdgeHint
            text: i18n.tr("Create entry")
            onClicked: revealBottomEdge()
            iconName: "note-new"
        }
    }

    Python {
        id: python

        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('../../src/'));
            setHandler('search_success', function (data) {
                activityIndicator.running = false
                for (var i = 0; i < data.length; i++) {
                    listmodel.append(data[i])
                }
            })

            setHandler('search_fail', function (error) {
                activityIndicator.running = false
                console.error(error)
            })

            setHandler('open_group_success', function (result) {
                if(result.path) {
                    actualPath = result.path
                } else {
                    actualPath = ""
                }
                for (var i = 0; i < result.groups_in_path.length; i++) {
                    listmodel.append(result.groups_in_path[i])
                }
                activityIndicator.running = false
            })

            setHandler('open_group_fail', function (error) {
                activityIndicator.running = false
                console.error(error)
            })

            setHandler('save_file_success', function () {
                console.log('Save success')
            })

            setHandler('save_file_fail', function (error) {
                console.error(error)
            })

            setHandler('close_database_success', function () {
                databaseOpen=false
                pageStack.push(Qt.resolvedUrl("../openFileTab/openFilePage.qml"))
            })

            setHandler('close_database_fail', function (error) {
                console.error(error)
            })

            importModule('keepass', function() {})
        }
        onError: {
            console.log('python error: ' + traceback);
        }

        function open_group(path=null) {
            listmodel.clear()
            activityIndicator.running = true
            if(!path) {
                searchEnable = false
            }
            python.call('keepass.open_group', [path ? path : null], function () {});
        }

        function search_entry(searh_name) {
            listmodel.clear()
            activityIndicator.running = true
            python.call('keepass.search_entry', [searh_name], function () {});
        }

        function close_database() {
            python.call('keepass.close_database', [], function () {});
        }
    }
    
}
