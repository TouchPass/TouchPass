import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
import QtQuick.Layouts 1.3
import io.thp.pyotherside 1.4

Item {
    id: newFileItem
    width: tabView.width
    height: tabView.height
    opacity: tabView.currentIndex == 1 ? 1 : 0

    UITK.Label {
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        width: parent.width
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        linkColor: theme.palette.normal.focus
        text: i18n.tr("TODO")
    }
}