import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
import QtQuick.Layouts 1.3
import io.thp.pyotherside 1.4
import Ubuntu.Content 1.3

Item {
    id: existingFileItem
    width: tabView.width
    height: tabView.height
    anchors.top: parent.top
    anchors.topMargin: units.gu(5)

    ColumnLayout {
        anchors.horizontalCenter: parent.horizontalCenter

        anchors.leftMargin: units.gu(7)
        anchors.rightMargin: units.gu(7)
        spacing: units.gu(1)

        UITK.Label {
            id: labelImportDatabase
            text: i18n.tr("Import local database")
            verticalAlignment: UITK.Label.AlignVCenter
            horizontalAlignment: UITK.Label.AlignHCenter
        }
        RowLayout {
            Layout.fillWidth: true
            UITK.TextField {
                enabled: false
                text: ''
                Layout.fillWidth: true
            }

            UITK.Button {
                id: pickDB
                text: i18n.tr("Select")
                enabled: !busy
                color: UITK.UbuntuColors.green
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("importPage.qml"),{"contentType": ContentType.All, "handler": ContentHandler.Source})
                    imported.connect(function(fileUrl) {
                        importedDB = fileUrl
                        python.copy_db()
                    })
                }
            }
        }
        UITK.ActivityIndicator {
            Layout.fillWidth: true
            running: busy
            visible: busy
        }
    }
}