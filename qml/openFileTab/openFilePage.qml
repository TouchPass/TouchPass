import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
import Ubuntu.Components.Popups 1.3 as UC
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4
import Ubuntu.Content 1.3
import "../components"

UITK.Page {
    id: pageOpenFile
    anchors.fill: parent
    objectName: "OpenDatabasePage"

    property string errorMessage
    property bool busy
    property string selectDB
    property string importedDB


    header: MainPageHeader {
        id: header
        pageTitle: i18n.tr("Open Database")
    }

    ListView {
        id: fileListView
        anchors {
            top: header.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        model: ListModel {
            id: listmodel
        }

        UITK.PullToRefresh {
            refreshing: busy
            onRefresh: python.get_database_files_list()
        }

        delegate: UITK.ListItem {
            id: databaseFileListItem

            height: databaseFileLayout.height + (divider.visible ? divider.height : 0)
            enabled: !busy

            onClicked: {
                selectDB = model.path
                PopupUtils.open(passwordPromptComponent)
            }

            UITK.ListItemLayout {
                id: databaseFileLayout
                title.text: model.title
                subtitle.text: model.type == 'local' ? i18n.tr("Local import database") : i18n.tr("Nextcloud file")
                summary.text: model.last

                UITK.Icon {
                    id: imageShape
                    UITK.SlotsLayout.position: UITK.SlotsLayout.Leading
                    height: units.gu(6)
                    width: height
                    asynchronous: true
                    source: "../../assets/keepass-icon.png"
                }
            }
            leadingActions: UITK.ListItemActions {
                id: databaseFileLeadingActions
                actions: [
                    UITK.Action {
                        iconName: "delete"
                        onTriggered: {
                            python.delete_database(model.path)
                        }
                        enabled: !busy
                    }
                ]
            }
            trailingActions: UITK.ListItemActions {
                id: databaseFileTrailingActions
                actions: [
                    UITK.Action {
                        iconName: "edit"
                        onTriggered: {
                            selectDB = model.path
                            PopupUtils.open(passwordPromptComponent)
                        }
                        enabled: !busy
                    },
                    UITK.Action {
                        iconName: "save-to"
                        onTriggered: {
                            pageStack.push(Qt.resolvedUrl("../exportPage/exportPage.qml"), { url: model.path })
                        }
                        enabled: !busy
                    }
                ]
            }
        }
        Component.onCompleted: {
            python.get_database_files_list()
        }
    }
    Item {
        anchors.fill: parent
        visible: listmodel.count == 0

        EmptyState {
            id: state
            anchors {
                top: parent.top
                topMargin: units.gu(16)
                left: parent.left
                right: parent.right
                margins: units.gu(4)
                verticalCenter: parent.verticalCenter
            }
            title: i18n.tr("Empty List")
            subTitle: i18n.tr("Please, import KeePass database in the bottom edge menu")
            iconName: "import"
        }
    }
    Rectangle {
        id: waitingItem
        anchors.fill: parent
        color: theme.palette.normal.background
        objectName: "waitingItem"
        visible: busy

        UITK.ActivityIndicator {
            id: activityIndicator
            anchors.centerIn: parent
            visible: busy
            running: busy
        }
    }

    UITK.BottomEdge {
        id: bottomEdge
        preloadContent: false
        height: root.height
        contentUrl: Qt.resolvedUrl("../openFileTab/bottomEdgeContent.qml")
        hint.visible: enabled

        UITK.BottomEdgeHint {
            id: bottomEdgeHint
            text: i18n.tr("Import KeePass")
            onClicked: revealBottomEdge()
            iconName: "import"
        }
    }
    Component {
        id: errorMessageComponent
        UC.Dialog {
            id: errorMessagePopup
            title: i18n.tr("Error on database opening")
            modal: true
            text: errorMessage
            UITK.Button {
                text: "Ok"
                color: UITK.UbuntuColors.red
                onClicked: {
                    PopupUtils.close(errorMessagePopup)
                }
            }
        }
    }

    Component {
        id: passwordPromptComponent
        UC.Dialog {
            id: passwordPromptPopup
            title: i18n.tr("Passphrase")
            modal: true
            text: i18n.tr("Enter passphrase to unlock KeePass database")
            RowLayout {
                Layout.fillWidth: true
                UITK.TextField {
                    id: password
                    enabled: !busy
                    text: ''
                    echoMode: showPasswordAction.checked ? TextInput.Normal : TextInput.Password
                    Layout.fillWidth: true
                }

                UITK.ActionBar {
                    numberOfSlots: 1
                    actions: [
                        UITK.Action {
                            id: showPasswordAction
                            checkable: true
                            iconName: checked ? "view-off" : "view-on"
                        }
                    ]
                }
            }
            RowLayout {
                Layout.fillWidth: true
                UITK.Button {
                    text: i18n.tr("Cancel")
                    color: UITK.UbuntuColors.red
                    onClicked: {
                        password.text = ""
                        PopupUtils.close(passwordPromptPopup)
                    }
                    visible: !busy
                }
                UITK.Button {
                    text: i18n.tr("Open")
                    color: UITK.UbuntuColors.green
                    onClicked: {
                        python.open_db(password.text)
                        PopupUtils.close(passwordPromptPopup)
                    }
                    visible: !busy
                }
            }

        }
    }
    Python {
        id: python

        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('../../src/'));

            setHandler('list_keepass_file_fail', function (reason) {
                busy = false
                console.log(reason)
                errorMessage = reason
                PopupUtils.open(errorMessageComponent)
            })

            setHandler('list_keepass_file_success', function (result) {
                busy = false
                listmodel.clear()
                for (var i = 0; i < result.length; i++) {
                    listmodel.append(result[i])
                }
            })

            setHandler('copy_db_fail', function (reason) {
                busy = false
                console.log(reason)
                errorMessage = reason
                PopupUtils.open(errorMessageComponent)
            })

            setHandler('copy_db_success', function (db_path) {
                busy = false
                bottomEdge.collapse()
                pageStack.push(Qt.resolvedUrl("openFilePage.qml"))
            })

            setHandler('db_open_fail', function (reason) {
                busy = false
                console.log(reason)
                errorMessage = reason
                PopupUtils.open(errorMessageComponent)
            })

            setHandler('db_open_sucess', function () {
                busy = false
                databaseOpen = true
                pageStack.push(Qt.resolvedUrl("../listEntryAndGroupTab/listEntryAndGroupTab.qml"))
            })

            setHandler('delete_database_file_fail', function (reason) {
                busy = false
                console.log(reason)
            })

            setHandler('delete_database_file_success', function () {
                python.get_database_files_list()
            })

            importModule('keepass', function() {})
        }
        onError: {
            console.log('python error: ' + traceback);
        }
        onReceived: {
            console.log('got message from python: ' + data);
        }

        function copy_db() {
            busy = true
            listmodel.clear()
            python.call('keepass.copy_db', [importedDB], function () {});
        }

        function open_db(password) {
            busy = true
            python.call('keepass.open_db', [selectDB, password], function () {});
        }

        function get_database_files_list() {
            busy = true
            python.call('keepass.list_keepass_file', [], function () {});
        }
        
        function delete_database(path) {
            busy = true
            python.call('keepass.delete_database_file', [path], function () {});
        }
    }
}