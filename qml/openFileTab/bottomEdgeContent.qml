import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
import QtQuick.Layouts 1.3
import io.thp.pyotherside 1.4
import QtQml.Models 2.1
import Ubuntu.Content 1.3

Rectangle {
    id: importDatabasePage
    height: bottomEdge.height
    width: bottomEdge.width
    color: theme.palette.normal.background
    objectName: "ImportDatabasePage"

    UITK.PageHeader {
        id: headerBottomEdge
        opacity: 1
        title: i18n.tr("Import or create database")
    }

    UITK.ScrollView {
        id: rootItem
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
            bottom: parent.bottom
            topMargin: headerBottomEdge.height + units.gu(2)
        }

        UITK.Sections {
            id: sections
            anchors {
                horizontalCenter: parent.horizontalCenter
            }

            actions: [
                UITK.Action {
                    text: i18n.tr("Existing database")
                },
                UITK.Action {
                    text: i18n.tr("New database")
                }
            ]
            onSelectedIndexChanged: {
                tabView.currentIndex = selectedIndex
            }

        }

        ListView {
            id: tabView
            anchors {
                top: parent.top
                bottom: parent.bottom
                left: parent.left
                right: parent.right
                topMargin: sections.height + units.gu(2)
            }
            currentIndex: 0

            orientation: Qt.Horizontal
            snapMode: ListView.SnapOneItem
            highlightRangeMode: ListView.StrictlyEnforceRange
            highlightMoveDuration: UITK.UbuntuAnimation.FastDuration

            onCurrentIndexChanged: {
                sections.selectedIndex = currentIndex
            }

            model: ObjectModel {
                Loader {
                    width: tabView.width
                    height: tabView.height
                    asynchronous: true
                    source: Qt.resolvedUrl("importExistingDatabase.qml")
                }
                Loader {
                    width: tabView.width
                    height: tabView.height
                    asynchronous: true
                    source: Qt.resolvedUrl("createDatabase.qml")
                }
            }
        }
    }
    Python {
        id: pythonBottomEdge

        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('../../src/'));

            setHandler('list_keepass_file_fail', function (reason) {
                busy = false
                console.log(reason)
                errorMessage = reason
                PopupUtils.open(errorMessageComponent)
            })

            setHandler('list_keepass_file_success', function (result) {
                busy = false
                for (var i = 0; i < result.length; i++) {
                    listmodel.append(result[i])
                }
            })

            importModule('keepass', function() {})
            
        }
        onError: {
            console.log('python error: ' + traceback);
        }
        onReceived: {
            console.log('got message from python: ' + data);
        }

        function launch_import_page() {
            pageStack.push(Qt.resolvedUrl("importPage.qml"),{"contentType": ContentType.All, "handler": ContentHandler.Source})
        }
        
    }
}