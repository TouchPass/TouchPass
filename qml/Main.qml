import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4
import QtQml.Models 2.1

UITK.MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'touchpass.touchpass'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    signal imported(string fileUrl)
    signal exported(string fileUrl)

    property bool databaseOpen: false
    property bool searchEnable : false
    property double lastHeartbeat: 0


    UITK.PageStack {
        id: pageStack
        anchors.fill: parent

        onCurrentPageChanged: {
            console.log("Current PageStack Page: " + currentPage.objectName)
        }
    }

    Settings {
        id: settings
        property string lastDB
        property int autoCloseInterval: 15
        property bool autoSaveFile: true
    }

    Component.onCompleted: {
        pageStack.push(Qt.resolvedUrl("openFileTab/openFilePage.qml"));
    }

    Timer {
        interval: 30000
        running: settings.autoCloseInterval > 0 && databaseOpen
        repeat: true
        onTriggered: {
            const now = new Date().getTime()
            if (lastHeartbeat === 0) {
                lastHeartbeat = now
            }

            lastHeartbeat = now
            if (databaseOpen
                    && delta >= settings.autoCloseInterval * 60 * 1000) {
                pageStack.pop(null)
            }
        }
    }

}
