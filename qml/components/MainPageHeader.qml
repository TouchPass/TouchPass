import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4


UITK.PageHeader {
    id: mainPageHeader
    property int selectedTabIndex: 0
    property string pageTitle

    leadingActionBar.actions: [
        UITK.Action {
            text: i18n.tr("Open Database")
            onTriggered: {
                pageStack.push(Qt.resolvedUrl("../openFileTab/openFilePage.qml"))
            }
            iconName: "document-open"
            enabled: !databaseOpen && !busy
            visible: !databaseOpen
        },
        UITK.Action {
            text: i18n.tr("Database content")
            onTriggered: {
                pageStack.push(Qt.resolvedUrl("../listEntryAndGroupTab/listEntryAndGroupTab.qml"))
            }
            iconName: "document-preview"
            visible: databaseOpen
            enabled: databaseOpen
        },
        UITK.Action {
            text: i18n.tr("Save")
            onTriggered: {
                python.call('keepass.save_file', [], function () {});
            }
            iconName: "media-flash-symbolic"
            visible: databaseOpen
            enabled: databaseOpen
        },
        UITK.Action {
            text: i18n.tr("Export")
            onTriggered: {
                pageStack.push(Qt.resolvedUrl("../exportPage/exportPage.qml"), { url: settings.lastDB })
            }
            visible: databaseOpen
            iconName: "document-save"
            enabled: databaseOpen
        },
        UITK.Action {
            text: i18n.tr("Settings")
            onTriggered: {
                pageStack.push(Qt.resolvedUrl("../settingsTab/settingsTab.qml"))
            }
            iconName: "settings"
            enabled: !busy
        },
        UITK.Action {
            text: i18n.tr("Close")
            onTriggered: {
                python.close_database()
            }
            visible: databaseOpen
            iconName: "close"
            enabled: databaseOpen
        }
    ]

    trailingActionBar {
        actions: [
            UITK.Action {
                id: aboutAction
                text: i18n.tr("About")
                iconName: "info"
                onTriggered: {
                    pageStack.push(Qt.resolvedUrl("../aboutTab/aboutPage.qml"))
                }
                enabled: !busy
            },
            UITK.Action {
                id: searchAction
                text: i18n.tr("Search")
                iconName: "toolkit_input-search"
                visible: databaseOpen
                onTriggered: {
                    searchEnable = !searchEnable
                    if(searchEnable) {
                        searchField.forceActiveFocus()
                    } else {
                        searchField.text = ''
                        python.open_group()
                    }
                }
            }                        
        ]
    }

    contents: Item {
        anchors.fill: parent

        UITK.TextField {
            id: searchField
            anchors.topMargin: units.gu(1)
            anchors.bottomMargin: units.gu(1)
            visible: searchEnable
            placeholderText: i18n.tr("Search")
            anchors.fill: parent
            inputMethodHints: Qt.ImhNoPredictiveText
            hasClearButton: true
            Keys.onReturnPressed: {
                python.search_entry(searchField.text)
            }
        }
        UITK.ListItemLayout {
            anchors.centerIn: parent
            anchors.verticalCenterOffset: units.gu(0.25)
            visible: searchEnable ? false : true
            title.text: i18n.tr("TouchPass")
            subtitle.text: pageTitle
        }
    }
}