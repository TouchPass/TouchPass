import QtQuick 2.4
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.1

ListItem {
    id: rootItem
    height: Math.max(implicitHeight, layout.height)

    property alias title: layout.title
    property alias subtitle: layout.subtitle
    property alias summary: layout.summary
    default property alias control: controlContainer.data

    ListItemLayout {
        id: layout
        anchors.centerIn: parent

        Item {
            id: controlContainer
            SlotsLayout.position: SlotsLayout.Last
            width: childrenRect.width
            height: childrenRect.height
            enabled: rootItem.enabled
        }
    }
}
