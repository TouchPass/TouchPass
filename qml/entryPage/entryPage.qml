import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.4
import Ubuntu.Content 1.3

UITK.Page {
    id: entryPagerootItem
    anchors.fill: parent
    objectName: "EditEntryPage"

    property var path
    property string entryTitle
    property string entryPassword
    property string entryUsername
    property string entryNotes
    property string entryURL
    property bool editMode: false
    property bool deleteMode : false
    property bool busy

    header: UITK.PageHeader {
        id: header
        opacity: 1
        title: entryTitle

        trailingActionBar.actions: [
            UITK.Action {
                iconName: "delete"
                text: i18n.tr("Delete")
                onTriggered: {
                    python.delete_entry()
                }
            },
            UITK.Action {
                iconName: "media-flash-symbolic"
                visible: editMode
                text: i18n.tr("Save")
                onTriggered: {
                    python.edit_entry()
                }
            },
            UITK.Action {
                iconName: "compose"
                visible: !editMode
                text: i18n.tr("Edit")
                onTriggered: {
                    editMode = !editMode
                }
            }
        ]
    }
    ScrollView {
        id: view
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: header.bottom
            bottom: parent.bottom
            topMargin: units.gu(5)
        }

        ColumnLayout {
            spacing: units.gu(1)

            Label {
                id: labelUsername
                text: i18n.tr("Username")
                verticalAlignment: Label.AlignVCenter
                horizontalAlignment: Label.AlignHCenter
            }
            RowLayout {
                Layout.fillWidth: true
                UITK.TextField {
                    id: username
                    enabled: editMode && !busy
                    text: entryUsername
                    Layout.fillWidth: true
                    onTextChanged: entryUsername = username.text
                }
                UITK.ActionBar {
                    numberOfSlots: 1
                    actions: [
                        UITK.Action {
                            id: copyUsername
                            parameterType: UITK.Action.None
                            iconName: "edit-copy"
                            onTriggered: {
                                UITK.Clipboard.push(entryUsername)
                            }
                        }
                    ]
                }
            }

            Label {
                id: labelPassword
                text: i18n.tr("Password")
                verticalAlignment: Label.AlignVCenter
                horizontalAlignment: Label.AlignHCenter
            }
            RowLayout {
                Layout.fillWidth: true
                UITK.TextField {
                    id: password
                    enabled: editMode && !busy
                    text: entryPassword
                    onTextChanged: entryPassword = password.text
                    echoMode: showPasswordAction.checked ? TextInput.Normal : TextInput.Password
                    Layout.fillWidth: true
                }

                UITK.ActionBar {
                    numberOfSlots: 2
                    actions: [
                        UITK.Action {
                            id: copyPassword
                            parameterType: UITK.Action.None
                            iconName: "edit-copy"
                            onTriggered: {
                                UITK.Clipboard.push(entryPassword)
                            }
                        },
                        UITK.Action {
                            id: showPasswordAction
                            checkable: true
                            iconName: checked ? "view-off" : "view-on"
                        }
                    ]
                }
            }

            Label {
                id: labelUrl
                text: i18n.tr("URL")
                verticalAlignment: Label.AlignVCenter
                horizontalAlignment: Label.AlignHCenter
            }
            RowLayout {
                Layout.fillWidth: true
                UITK.TextField {
                    id: url
                    enabled: editMode && !busy
                    text: entryURL
                    Layout.fillWidth: true
                    onTextChanged: entryURL = url.text
                }
                UITK.ActionBar {
                    numberOfSlots: 2
                    actions: [
                        UITK.Action {
                            id: openUrl
                            parameterType: UITK.Action.None
                            iconName: "external-link"
                            onTriggered: {
                                if (entryURL.indexOf('//') === -1) {
                                    Qt.openUrlExternally('http://' + entryURL)
                                    return
                                }

                                Qt.openUrlExternally(entryURL)
                            }
                        },
                        UITK.Action {
                            id: copyUrl
                            parameterType: UITK.Action.None
                            iconName: "edit-copy"
                            onTriggered: {
                                UITK.Clipboard.push(entryURL)
                            }
                        }
                    ]
                }
            }

            Label {
                id: labelNotes
                text: i18n.tr("Notes")
                verticalAlignment: Label.AlignVCenter
                horizontalAlignment: Label.AlignHCenter
            }
            RowLayout {
                Layout.fillWidth: true
                UITK.TextArea {
                    id: notes
                    enabled: editMode && !busy
                    width: units.gu(20)
                    height: units.gu(15)
                    contentWidth: units.gu(20)
                    contentHeight: units.gu(15)
                    text: entryNotes
                    onTextChanged: entryNotes = notes.text
                }
            }
            UITK.ActivityIndicator {
                Layout.fillWidth: true
                running: busy
                visible: busy
            }
        }
    }
    
    Python {
        id: python

        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('../../src/'));
            setHandler('open_entry_success', function (data) {
                entryTitle = data.title
                entryPassword = data.password
                entryNotes = data.notes
                entryURL = data.url
                entryUsername = data.username
            })

            setHandler('open_entry_fail', function (error) {
                console.error(error)
            })

            setHandler('edit_entry_success', function () {
                editMode = !editMode
                if(settings.autoSaveFile) {
                    python.call('keepass.save_file', [], function () {});
                } else {
                    busy = false
                }
            })

            setHandler('edit_entry_fail', function (error) {
                console.error(error)
            })

            setHandler('save_file_success', function () {
                busy = false
                if(deleteMode) {
                    deleteMode=false
                    pageStack.pop()
                }
            })

            setHandler('save_file_fail', function (error) {
                busy = false
                console.error(error)
            })

            setHandler('delete_entry_success', function () {
                if(settings.autoSaveFile) {
                    python.call('keepass.save_file', [], function () {});
                } else {
                    busy = false
                    deleteMode = false
                    pageStack.pop()
                }
            })

            setHandler('delete_entry_fail', function (error) {
                busy = false
                console.error(error)
            })

            importModule('keepass', function() {
                console.log('Python lib imported')
                python.call('keepass.open_entry', [path], function () {});
            })
        }
        onError: {
            console.log('python error: ' + traceback);
        }
        
        function edit_entry() {
            busy = true
            python.call('keepass.edit_entry', [path, entryTitle, entryPassword, entryNotes, entryURL, entryUsername], function () {});
        }

        function delete_entry() {
            busy = true
            deleteMode = true
            python.call('keepass.delete_entry', [path], function () {});
        }
    }

}
