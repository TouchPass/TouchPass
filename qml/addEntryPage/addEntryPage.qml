import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
import QtQuick.Layouts 1.3
import io.thp.pyotherside 1.4

UITK.Page {
    id: addEntryPageItem
    height: bottomEdge.height
    objectName: "AddEntryPage"

    property string entryTitle
    property string entryPassword
    property string entryUsername
    property string entryNotes
    property string entryURL
    property bool busy

    header: UITK.PageHeader {
        id: header
        opacity: 1
        title: i18n.tr("Create new entry")

        trailingActionBar.actions: [
            UITK.Action {
                iconName: "ok"
                text: i18n.tr("Create")
                onTriggered: {
                    python.create_entry()
                }
            }
        ]
    }


    Item {
        anchors.fill: parent

        UITK.ScrollView{
            id: view
            width: columnLayout.width
            anchors {
                horizontalCenter: parent.horizontalCenter
                top: parent.top
                bottom: parent.bottom
                topMargin: header.height + units.gu(2)
            }

            ColumnLayout {
                id: columnLayout
                spacing: units.gu(1)

                UITK.Label {
                    id: labelTitle
                    text: i18n.tr("Title")
                    verticalAlignment: UITK.Label.AlignVCenter
                    horizontalAlignment: UITK.Label.AlignHCenter
                }
                RowLayout {
                    Layout.fillWidth: true
                    UITK.TextField {
                        id: title
                        enabled: !busy
                        text: ''
                        Layout.fillWidth: true
                        onTextChanged: entryTitle = title.text
                    }
                }

                UITK.Label {
                    id: labelUsername
                    text: i18n.tr("Username")
                    verticalAlignment: UITK.Label.AlignVCenter
                    horizontalAlignment: UITK.Label.AlignHCenter
                }
                RowLayout {
                    Layout.fillWidth: true
                    UITK.TextField {
                        id: username
                        enabled: !busy
                        text: ''
                        Layout.fillWidth: true
                        onTextChanged: entryUsername = username.text
                    }
                }

                UITK.Label {
                    id: labelPassword
                    text: i18n.tr("Password")
                    verticalAlignment: UITK.Label.AlignVCenter
                    horizontalAlignment: UITK.Label.AlignHCenter
                }
                RowLayout {
                    Layout.fillWidth: true
                    UITK.TextField {
                        id: password
                        enabled: !busy
                        text: ''
                        onTextChanged: entryPassword = password.text
                        echoMode: showPasswordAction.checked ? TextInput.Normal : TextInput.Password
                        Layout.fillWidth: true
                    }

                    UITK.ActionBar {
                        numberOfSlots: 1
                        actions: [
                            UITK.Action {
                                id: showPasswordAction
                                checkable: true
                                iconName: checked ? "view-off" : "view-on"
                            }
                        ]
                    }
                }

                UITK.Label {
                    id: labelUrl
                    text: i18n.tr("URL")
                    verticalAlignment: UITK.Label.AlignVCenter
                    horizontalAlignment: UITK.Label.AlignHCenter
                }
                RowLayout {
                    Layout.fillWidth: true
                    UITK.TextField {
                        id: url
                        enabled: !busy
                        text: ''
                        Layout.fillWidth: true
                        onTextChanged: entryURL = url.text
                    }
                }
                UITK.Label {
                    id: labelNotes
                    text: i18n.tr("Notes")
                    verticalAlignment: UITK.Label.AlignVCenter
                    horizontalAlignment: UITK.Label.AlignHCenter
                }
                RowLayout {
                    Layout.fillWidth: true
                    UITK.TextArea {
                        id: notes
                        enabled: !busy
                        width: units.gu(20)
                        height: units.gu(15)
                        contentWidth: units.gu(20)
                        contentHeight: units.gu(15)
                        text: ''
                        onTextChanged: entryNotes = notes.text
                    }
                }
                UITK.ActivityIndicator {
                    Layout.fillWidth: true
                    running: busy
                    visible: busy
                }
            }
        }
    }
    Python {
        id: python

        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('../../src/'));

            setHandler('create_entry_success', function () {
                if(settings.autoSaveFile) {
                    python.call('keepass.save_file', [], function () {});
                } else {
                    busy = false
                    bottomEdge.collapse()
                }
            })

            setHandler('create_entry_fail', function (error) {
                console.error(error)
            })

            setHandler('save_file_success', function () {
                busy = false
                bottomEdge.collapse()
            })

            setHandler('save_file_fail', function (error) {
                busy = false
                console.error(error)
            })

            importModule('keepass', function() {})
        }
        onError: {
            console.log('python error: ' + traceback);
        }        
        function create_entry() {
            busy = true
            python.call('keepass.create_entry', [actualPath, entryTitle, entryPassword, entryNotes, entryURL, entryUsername], function () {});
        }
    }
}
