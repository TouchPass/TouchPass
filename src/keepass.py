import pyotherside
import pykeepass
import shutil
import os
import datetime

kp_pointer = None


def copy_db(original_db_path):
    try:
        target_directory = "/home/phablet/.local/share/touchpass.touchpass"
        if not os.path.exists(target_directory):
            os.mkdir(target_directory)

        original_db_path= original_db_path[7:]
        db_filename = original_db_path.split('/', 8 )[7]

        new_location=target_directory+"/{}".format(db_filename)
        shutil.copyfile(original_db_path, new_location)
        pyotherside.send('copy_db_success', new_location)
        return new_location
    except shutil.SameFileError:
        print("Source and destination represents the same file.")
        pyotherside.send('copy_db_fail', "Source and destination represents the same file.")
    except IsADirectoryError:
        print("Destination is a directory.")
        pyotherside.send('copy_db_fail', "Destination is a directory.")
    except PermissionError:
        print("Permission denied.")
        pyotherside.send('copy_db_fail', "Permission denied.")
    except Exception as e:
        print("Exception", e)
        pyotherside.send('copy_db_fail', str(e))
        return 'copy_db_fail'

def open_db(db_path, password):
    global kp_pointer
    try:
        kp_pointer = pykeepass.PyKeePass(db_path, password)
        pyotherside.send('db_open_sucess')
        return 'db_open_sucess'
    except pykeepass.exceptions.CredentialsError as e:
        pyotherside.send('db_open_fail', "Bad Credentials")
        return 'db_open_fail'
    except Exception as e:
        print("Exception", e)
        pyotherside.send('db_open_fail', str(e))
        return 'db_open_fail'

def open_group(group_path=None):
    group_to_return = []
    entries_to_return = []
    try:
        if not group_path:
            all_groups = kp_pointer.groups
            for group in all_groups:
                if len(group.path)==1:
                    group_to_return.append({'title': group.name, 'description': 'Toto', 'type': "group", 'path': str(group.path)})
                    group_to_return = sorted(group_to_return, key=lambda d: d['title'])
        else:
            path = eval(group_path)
            root_group = kp_pointer.find_groups (path=path, recursive=False, first=False)

            if root_group:
                previous_path = path
                previous_path.pop()
                if len(previous_path) == 0 :
                    previous_path = None
                else:
                    previous_path = str(previous_path)
                group_to_return.append({'title': "Back", 'type': "back", 'path': previous_path})
                for group in root_group.subgroups:
                    group_to_return.append({'title': group.name, 'description': 'Toto', 'type': "group", "path": str(group.path)})
                group_to_return = sorted(group_to_return, key=lambda d: d['title'])
                for entry in root_group.entries:
                    entries_to_return.append({'title': entry.title, 'username': entry.username, 'password': entry.password, 'type': "entry", 'path': str(entry.path)})
                entries_to_return = sorted(entries_to_return, key=lambda d: d['title'])
                group_to_return = group_to_return+entries_to_return

        pyotherside.send('open_group_success', {'path': group_path, 'groups_in_path': group_to_return})
        return 'open_group_success'
    except Exception as e:
        print("Exception", e)
        pyotherside.send('open_group_fail', e)
        return 'open_group_fail'


def open_entry(entry_path):
    try:
        entry = kp_pointer.find_entries(path=eval(entry_path), recursive=False, first=False)
        entry_to_return = {'title': entry.title if entry.title else '', 'username': entry.username if entry.username else '', 'password': entry.password if entry.password else '', 'notes': entry.notes if entry.notes else '', 'url': entry.url if entry.url else ''}
        pyotherside.send("open_entry_success", entry_to_return)
        return entry_to_return
    except Exception as e:
        print("Exception", e)
        pyotherside.send('search_fail', e)
        return 'open_entry_fail'


def edit_entry(path, title, password, notes, url, username):
    global kp_pointer
    try:
        entry = kp_pointer.find_entries(path=eval(path), recursive=False, first=False)
        entry.title = title
        entry.password = password
        entry.notes = notes
        entry.url = url
        entry.username = username
        # kp_pointer.save()
        pyotherside.send('edit_entry_success')
        return 'edit_entry_success'
    except Exception as e:
        print("Exception", e)
        pyotherside.send('edit_entry_fail', str(e))
        return 'edit_entry_fail'


def save_file():
    global kp_pointer
    try: 
        kp_pointer.save()
        pyotherside.send('save_file_success')
        return 'save_file_success'
    except Exception as e:
        print("Exception", e)
        pyotherside.send('save_file_fail', str(e))
        return 'save_file_fail'


def search_entry(name):
    try:
        print(name)
        entries = kp_pointer.find_entries(title=".*"+name+".*", recursive=True, first=False, flags=["i"], regex=True)
        entries_to_return = []
        entries_to_return.append({'title': "Back", 'type': "back", 'path': None})
        for entry in entries:
            entries_to_return.append({'title': entry.title, 'username': entry.username, 'password': entry.password, 'type': "entry", 'path': str(entry.path)})
        print(entries_to_return)
        pyotherside.send('search_success', entries_to_return)
        return 'search_success'
    except Exception as e:
        print("Exception", e)
        pyotherside.send('search_fail', str(e))
        return 'search_fail'


def create_entry(path, title, password, notes, url, username):
    global kp_pointer
    try:
        group = kp_pointer.find_groups(path=eval(path), first=True)
        kp_pointer.add_entry(group, title, username, password, url=url, notes=notes)
        pyotherside.send('create_entry_success')
        return 'create_entry_success'
    except Exception as e:
        print("Exception", e)
        pyotherside.send('create_entry_fail', str(e))
        return 'create_entry_fail'


def delete_entry(path):
    global kp_pointer
    try:
        entry = kp_pointer.find_entries(path=eval(path), first=True)
        kp_pointer.delete_entry(entry)
        pyotherside.send('delete_entry_success')
        return 'delete_entry_success'
    except Exception as e:
        print("Exception", e)
        pyotherside.send('delete_entry_fail', str(e))
        return 'delete_entry_fail'


def list_keepass_file():
    try:
        keepass_directory = "/home/phablet/.local/share/touchpass.touchpass"
        if not os.path.exists(keepass_directory):
            os.mkdir(keepass_directory)

        keepass_file_list = []
        for file in os.listdir(keepass_directory):
            if file.endswith('.kdbx'):
                path = keepass_directory+"/"+file
                keepass_file_list.append({"title": file, "path": path, "type": "local", "last": datetime.datetime.fromtimestamp(os.path.getmtime(path))})
        pyotherside.send('list_keepass_file_success', keepass_file_list)
        return 'list_keepass_file_success'
    except Exception as e:
        print("Exception", e)
        pyotherside.send('list_keepass_file_fail', str(e))
        return 'list_keepass_file_fail'
    except IsADirectoryError:
        print("Destination is a directory.")
        pyotherside.send('copy_db_fail', "Destination is a directory.")
    except PermissionError:
        print("Permission denied.")
        pyotherside.send('copy_db_fail', "Permission denied.")


def delete_database_file(path):
    try:
        os.remove(path)
        pyotherside.send('delete_database_file_success')
        return 'delete_database_file_success'
    except Exception as e:
        print("Exception", e)
        pyotherside.send('delete_database_file_fail', str(e))
        return 'delete_database_file_fail'

def close_database():
    global kp_pointer
    try:
        kp_pointer=None
        pyotherside.send('close_database_success')
        return 'close_database_success'
    except Exception as e:
        print("Exception", e)
        pyotherside.send('close_database_fail', str(e))
        return 'close_database_fail'