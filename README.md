# TouchPass

KeePass application for Ubuntu Touch.
This application is inspired by the already existing application [KeePass](https://open-store.io/app/keepass.davidv.dev) by [David Ventura](https://github.com/DavidVentura).

It uses the Python library [pykeepass](https://github.com/libkeepass/pykeepass) which is slower than that of the original [KeePass](https://open-store.io/app/keepass.davidv.dev) application to open the database, but it allows to modify entries (future feature).

Application functionnal on Vollaphone 22 and Ubuntu Touch 16.04.

## Features

### Actual
- Support kdbx as per [pykeepass](https://github.com/libkeepass/pykeepass)
- Search in entries
- Open/Copy URL from entry
- Copy user/password to clipboard
- Entry entry
- Add new entry
- Delete entry
- Import/Export database to device Documents
- Autosaving modification
- Auto-close database after inactive time

### TODO

- Create new database
- Synchronise database with Nextcloud account
- Group management

## Screenshots

![](./Screenshots/capture-1.png)
![](./Screenshots/capture-2.png)
![](./Screenshots/capture-3.png)
![](./Screenshots/capture-4.png)
![](./Screenshots/capture-5.png)
![](./Screenshots/capture-6.png)
![](./Screenshots/capture-7.png)
![](./Screenshots/capture-8.png)

## Build

Get the latest version of the application
```bash
git clone https://gitea.verbaere.com/TouchPass/TouchPass.git
```

To build the app, we need to add Python libs, to do that we need arm64 env to download and build them with pip
Enable arm64 emulation with Qemu like discribe [here](https://www.stereolabs.com/docs/docker/building-arm-container-on-x86/)
```bash
sudo apt-get install qemu binfmt-support qemu-user-static # Install the qemu packages
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes # This step will execute the registering scripts

docker run --rm -t arm64v8/ubuntu:16.04 uname -m # Testing the emulation environment
```

Run docker container with Ubuntu 16.04 to have the right Python version
```bash
docker run --rm -v <PATH_TO_SRC_DIR_OF_APPLICATION>:/root/work -it arm64v8/ubuntu:16.04 bash
```

Install dependancies to build libs
```bash
apt update
apt install python3 curl libffi-dev python3-cffi build-essential python3-dev libxml2 libxml2-dev libxslt1-dev zlib1g-dev libxmu-dev libxmu-headers freeglut3-dev libxext-dev libxi-dev
```

Install latest available pip version
```bash
curl -fsSL https://bootstrap.pypa.io/pip/3.5/get-pip.py | python3.5
```

Install and build python libs to share directory and then exit container
```bash
pip3 install pykeepass -t /root/work
exit
```

Go inside app directory
```bash
CLICKABLE_FRAMEWORK=ubuntu-sdk-16.04.3 clickable --arch arm64
```


## License

Copyright (C) 2023  Pierre VERBAERE

Licensed under the MIT license
